package main

import (
	"fmt"
	"io"
	"net/url"
	"strconv"
	"strings"

	//"net/http"
	//"io"
	"log"
	//"os"
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

//var a *url.URL
var refs string
var schema_format string
var SchemaPath string

func path_nethod(url *url.URL, method string, reader io.Reader) {
	if validation(a, method) {
		Test_vali(refs, reader)
	} else {
		fmt.Println("Validation Failed")
	}

}

func validation(url *url.URL, method string) bool {

	SchemaPath := "./openapi.yaml" //fixed

	input_url := strings.Split(url.String(), "/") //Cut string

	schema_format = fmt.Sprintf("/%s/%s", input_url[3], input_url[4])
	schema_format = strings.Replace(schema_format, "%7B", "{", -1) //{
	schema_format = strings.Replace(schema_format, "%7D", "}", -1) //}

	//--------------read yaml------------------
	var t map[interface{}]interface{}
	t = make(map[interface{}]interface{})

	buffer, err := ioutil.ReadFile(SchemaPath) //read yaml
	if err != nil {
		log.Fatalf(err.Error())
	}

	err = yaml.Unmarshal(buffer, &t) //Unmarshal yaml to json and assign to map[interface{}]interface{}

	if err != nil {
		panic(err)
	}
	//-------------------------------------------

	test := t["paths"].(map[interface{}]interface{})
	var t2 map[string]interface{}
	t2 = make(map[string]interface{})
	if PathCheck(test) {
		t2 = PathConvert(test)

		format := t2[schema_format].(map[string]interface{})
		methods := format[method].(map[string]interface{})
		requestBody := methods["requestBody"].(map[string]interface{})
		content := requestBody["content"].(map[string]interface{})

		validator(content)

		return true
	} else {
		fmt.Println("Unable to find the corresponding format")
		return false
	}

}
func PathCheck(in map[interface{}]interface{}) bool {
	for key := range in {
		if PathJudge(key.(string)) {
			return true
		}
	}
	return false
}
func PathJudge(in string) bool {

	input_in := strings.Split(in, "/") //Cut string
	input_schema_format := strings.Split(schema_format, "/")

	if len(input_schema_format) > len(input_in) {
		return false
	}
	var i int
	var j int
	for i = 0; i < len(input_in) && j < len(input_schema_format); i++ {

		if strings.Contains(input_in[i], "{") && strings.Contains(input_in[i], "}") {
			j++
			continue
		} else if input_in[i] != input_schema_format[j] {
			return false
		}
		j++
	}
	schema_format = in
	return true
}

func PathConvert(in map[interface{}]interface{}) map[string]interface{} {

	var out map[string]interface{}
	out = make(map[string]interface{})

	for key, value := range in {

		switch x := value.(type) {
		case map[interface{}]interface{}:
			switch y := key.(type) {
			case int:
				var t string
				t = strconv.Itoa(y)
				out[t] = PathConvert(x)
			case string:
				out[key.(string)] = PathConvert(x)
			}
		case []string:
			out[key.(string)] = value.([]string)
		case int:
			out[key.(string)] = value.(int)
		case string:
			out[key.(string)] = value.(string)
		case bool:
			out[key.(string)] = value.(bool)
		case []interface{}:
			out[key.(string)] = value.([]interface{})
		default:
			fmt.Printf("no key:  ")
			fmt.Println(key)
			fmt.Printf("no:   ")
			fmt.Println(x)
		}
	}
	return out
}

func validator(in map[string]interface{}) map[string]interface{} {

	var out map[string]interface{}
	out = make(map[string]interface{})

	for key, value := range in {

		switch x := value.(type) {
		case map[string]interface{}:
			out[key] = validator(x)
		case string:
			if key == "$ref" {
				refs = value.(string)
				break
			}
		default:
			fmt.Printf("no key:  ")
			fmt.Println(key)
			fmt.Printf("no:   ")
			fmt.Println(x)
		}
	}
	return out
}
