package main

import (
	"fmt"
	//"encoding/json"

    "log"
    //"strings"
    "github.com/xeipuuv/gojsonschema"
    "gopkg.in/yaml.v2"
    "io/ioutil"
    //"github.com/ghodss/yaml"
)


func main() {
    var t map[interface{} ]interface{} 
    t = make(map[interface{} ]interface{})
    //var t = make(map[string]gojsonschema.JSONLoader)
    //var t interface{}
	buffer, err := ioutil.ReadFile("./TS29510_Nnrf_NFManagement.yaml")
	if err != nil {
        log.Fatalf(err.Error())
    }

    err = yaml.Unmarshal(buffer, &t)
	
    if err != nil {
        panic(err)
    }
  
	var t2 map[string]interface{} 
    t2 = make(map[string]interface{})
	t2 = convert2(t)
	
	fmt.Println(t2)

	schema := gojsonschema.NewGoLoader(t2)

    documentLoader := gojsonschema.NewReferenceLoader("file:///home/bbb/Downloads/test/document.json")

    if err != nil {
        log.Fatalf(err.Error())
    }
    
    result, err2 :=  gojsonschema.Validate(schema, documentLoader)
	
    if err2 != nil {
        panic(err.Error())
    }

    if result.Valid() {
        fmt.Printf("The document is valid\n")
    } else {
        fmt.Printf("The document is not valid. see errors :\n")
        for _, desc := range result.Errors() {
            fmt.Printf("- %s\n", desc)
        }
    }
}

func convert(i interface{}) interface{} {
    switch x := i.(type) {
    case map[interface{}]interface{}:
        m2 := map[string]interface{}{}
        for k, v := range x {
            m2[k.(string)] = convert(v)
        }
        return m2
    case []interface{}:
        for i, v := range x {
            x[i] = convert(v)
        }
    
    }
    return i
}

func convert2(in map[interface{}]interface{}) map[string]interface{} {

    var out map[string]interface{} 
    out = make(map[string]interface{})

    for key, value := range in {
       
		switch x := value.(type){
            case map[interface{}]interface{}:
                fmt.Print("key:  ")
                fmt.Println(key)
                fmt.Printf("value: ")
                fmt.Println(value)
				out[key.(string)] = convert2(x)
			case int:
				out[key.(string)] = value.(int)
				fmt.Printf("int key:  " );
				fmt.Println(key)
				fmt.Printf("int:  " );
				fmt.Println(value.(int))
			case string:
				out[key.(string)] = value.(string)
				fmt.Printf("string key:  " );
				fmt.Println(key)
				fmt.Printf("string:   " );
				fmt.Println(value.(string))
			case bool:
				out[key.(string)] = value.(bool)
				fmt.Printf("bool key:  " );
				fmt.Println(key)
				fmt.Printf("bool:   " );
				fmt.Println(value.(bool))
            default:
				fmt.Printf("no key:  " );
				fmt.Println(key)
				fmt.Printf("no:   " );
				fmt.Println(x)
		}
	}
    return out
}

func convert3(in map[interface{}]interface{}) {

}