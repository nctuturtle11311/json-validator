//main 29503
package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"strings"

	//"os"
	"github.com/xeipuuv/gojsonschema"
	"gopkg.in/yaml.v2"
)

var reference []string
var referenceMap map[string][]string
var schema string
var NF map[string]interface{}

func Test_vali(s string, document io.Reader) {
	buf := make([]byte, 20000)
	for {
		n, _ := document.Read(buf)

		if 0 == n {
			break
		}

		//os.Stdout.Write(buf[:n])
	}
	//schema = "NFProfile"
	//schema = "PpActiveTime"
	input_schema := strings.Split(s, "/")
	schema = input_schema[3]

	referenceMap = make(map[string][]string)

	var t map[interface{}]interface{}
	t = make(map[interface{}]interface{})

	buffer, err := ioutil.ReadFile("./openapi.yaml") //read yaml
	//buffer, err := ioutil.ReadFile("./TS29503.yaml")//read yaml
	if err != nil {
		log.Fatalf(err.Error())
	}

	err = yaml.Unmarshal(buffer, &t) //Unmarshal yaml to json and assign to map[interface{}]interface{}

	if err != nil {
		panic(err)
	}

	test := t["components"].(map[interface{}]interface{})
	var t2 map[string]interface{}
	t2 = make(map[string]interface{})
	t2 = convert2(test) // convert map[interface{}]interface{} to map[string]interface{} recursively

	NF = t2["schemas"].(map[string]interface{})
	nfProfile := NF[schema].(map[string]interface{})

	delete(NF, schema) //BUG

	NF = read_external_ref(NF) // handle 'TS29571_CommonData.yaml#/components/schemas/NfInstanceId'

	nfProfile["definitions"] = NF //put into definitions

	if err != nil {
		fmt.Println("1")
		log.Fatalf(err.Error())
	}

	NFProfile := gojsonschema.NewGoLoader(nfProfile)
	s1 := gojsonschema.NewSchemaLoader()

	if err != nil {
		fmt.Println("2")
		log.Fatalf(err.Error())
	}

	schema2, err := s1.Compile(NFProfile)

	if err != nil {
		fmt.Println("3")
		log.Fatalf(err.Error())
	}

	//documentLoader := gojsonschema.NewReferenceLoader("file://./document.json")
	//documentLoader := gojsonschema.NewGoLoader(document)
	documentLoader := gojsonschema.NewStringLoader(string(buf))
	if err != nil {
		log.Fatalf(err.Error())
	}

	result, err2 := schema2.Validate(documentLoader)
	//result, err2 :=  gojsonschema.Validate(schema, documentLoader)

	if err2 != nil {
		panic(err.Error())
	}

	if result.Valid() {
		fmt.Printf("The document is valid\n")
	} else {
		fmt.Printf("The document is not valid. see errors :\n")
		for _, desc := range result.Errors() {
			fmt.Printf("- %s\n", desc)
		}
	}
}

func convert2_(i interface{}) interface{} {
	switch x := i.(type) {
	case map[interface{}]interface{}:
		m2 := map[string]interface{}{}
		for k, v := range x {
			m2[k.(string)] = convert2_(v)
		}
		return m2
	case string:
		if strings.Contains(i.(string), "#/components/schemas") {
			i = ref(i.(string)) //check whether there are reference
		}
		return i.(string)
	case []string:
		return i.([]string)
	case int:
		return i.(int)
	case bool:
		return i.(bool)
	case []interface{}:
		for i, v := range x {
			x[i] = convert2_(v)
		}
	}
	return i
}
func convert3_(i interface{}) interface{} {
	switch x := i.(type) {
	case map[interface{}]interface{}:
		m2 := map[string]interface{}{}
		for k, v := range x {
			m2[k.(string)] = convert3_(v)
		}
		return m2
	case string:
		if strings.Contains(i.(string), "#/components/schemas") {
			i = ref2(i.(string)) //check whether there are reference
		}
		return i.(string)
	case []string:
		return i.([]string)
	case int:
		return i.(int)
	case bool:
		return i.(bool)
	case []interface{}:
		for i, v := range x {
			x[i] = convert3_(v)
		}

	}
	return i
}
func convert2(in map[interface{}]interface{}) map[string]interface{} {

	var out map[string]interface{}
	out = make(map[string]interface{})

	for key, value := range in {

		switch x := value.(type) {
		case map[interface{}]interface{}:
			out[key.(string)] = convert2(x)
		case int:
			out[key.(string)] = value.(int)
		case string:
			if key.(string) == "$ref" {
				value = ref(value.(string)) //check whether there are reference
			}

			out[key.(string)] = value.(string)
		case []string:
			out[key.(string)] = value.([]string)
		case bool:
			out[key.(string)] = value.(bool)
		case []interface{}:
			//fmt.Println("convert2222222222222222222222222222222222222222222")
			//
			/*var i int
			if key.(string) == "enum" || key.(string) == "tag" {
				out[key.(string)] = value.([]interface{})
			} else if key.(string) == "required" {
				out[key.(string)] = value.([]interface{})
			} else {
				for i = 0; i < len(x); i++ {
					x[i] = convert2(x[i].(map[interface{}]interface{}))
				}
				out[key.(string)] = value.([]interface{})
			}*/
			var i int
			for i = 0; i < len(x); i++ {
				//fmt.Printf("[%d] value:   ", i)
				//fmt.Println(x[i])
				x[i] = convert2_(x[i])
			}
			out[key.(string)] = value.([]interface{})

		default:
			fmt.Printf("no key:  ")
			fmt.Println(key)
			fmt.Printf("no:   ")
			fmt.Println(x)
		}
	}
	return out
}
func convert3(in map[interface{}]interface{}) map[string]interface{} {

	var out map[string]interface{}
	out = make(map[string]interface{})

	for key, value := range in {

		switch x := value.(type) {
		case map[interface{}]interface{}:
			out[key.(string)] = convert3(x)
		case int:
			out[key.(string)] = value.(int)
		case string:
			if key.(string) == "$ref" {
				value = ref2(value.(string)) //check whether there are reference
			}
			out[key.(string)] = value.(string)
		case []string:
			out[key.(string)] = value.([]string)
		case bool:
			out[key.(string)] = value.(bool)
		case []interface{}:
			//fmt.Println("convert33333333333333333333333333333333333333333333333333333")
			/*var i int
			if key.(string) == "enum" || key.(string) == "tag" {
				out[key.(string)] = value.([]interface{})
			} else if key.(string) == "required" || key.(string) == "tags" {
				out[key.(string)] = value.([]interface{})
			} else {
				for i = 0; i < len(x); i++ {
					x[i] = convert3(x[i].(map[interface{}]interface{}))
				}
				out[key.(string)] = value.([]interface{})
			}*/
			var i int
			for i = 0; i < len(x); i++ {
				//fmt.Printf("[%d] value:   ", i)
				//fmt.Println(x[i])
				x[i] = convert3_(x[i])
			}
			out[key.(string)] = value.([]interface{})

		default:
			fmt.Printf("no key:  ")
			fmt.Println(key)
			fmt.Printf("no:   ")
			fmt.Println(x)
		}
	}
	return out
}
func ref(in string) string {

	input := strings.Split(in, "/")
	//'TS29571_CommonData.yaml#/components/schemas/NfInstanceId'
	// -> TS29571_CommonData.yaml#, components, schemas, NfInstanceId

	if input[3] == schema {
		//fmt.Println("Bugggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg")
		return ""
	} else if input[0] == "#" { //'#/components/schemas/ReferenceId'
		var s string
		s = fmt.Sprintf("#/%s/%s", "definitions", input[3])
		return s
	} else { //'TS29571_CommonData.yaml#/components/schemas/NfInstanceId'
		var s string
		s = fmt.Sprintf("#/%s/%s", "definitions", input[3])
		input[0] = strings.Replace(input[0], "#", "", 1)
		//fmt.Printf("%s : %s\n", input[0], input[3])
		referenceMap[input[0]] = append(referenceMap[input[0]], input[3])
		reference = append(reference, input[3]) //store into reference

		return s
	}

}
func ref2(in string) string {

	input := strings.Split(in, "/")
	//'TS29571_CommonData.yaml#/components/schemas/NfInstanceId'
	// -> TS29571_CommonData.yaml#, components, schemas, NfInstanceId

	if input[3] == schema {
		fmt.Println("Bugggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg")
		return ""
	} else if input[0] == "#" { //'#/components/schemas/ReferenceId'
		var s string
		s = fmt.Sprintf("#/%s/%s", "definitions", input[3])
		if !(Contains(reference, input[3])) {
			reference = append(reference, input[3])
		}
		return s
	} else { //'TS29571_CommonData.yaml#/components/schemas/NfInstanceId'
		//fmt.Println("ref2222222222222222222")
		var s string
		s = fmt.Sprintf("#/%s/%s", "definitions", input[3])
		reference = append(reference, input[3]) //store into reference
		return s
	}
	/*
		else if input[0] == "TS29518_Namf_Communication.yaml#" {
			return ""
		}
	*/
}
func read_external_ref(NF map[string]interface{}) map[string]interface{} {
	//fmt.Println("--------------read_external_ref-----------------")

	for i := range referenceMap {
		var r map[interface{}]interface{}
		r = make(map[interface{}]interface{})

		var fileName string
		fileName = fmt.Sprintf("%s%s", "./", i)

		buffer2, err := ioutil.ReadFile(fileName)

		if err != nil {
			log.Fatalf(err.Error())
		}

		err = yaml.Unmarshal(buffer2, &r)

		if err != nil {
			panic(err)
		}

		var r2 map[string]interface{}
		r2 = make(map[string]interface{})

		r2 = convert3(r)

		rcomponents := r2["components"].(map[string]interface{})
		rNF := rcomponents["schemas"].(map[string]interface{})

		for j := range referenceMap[i] {
			NF[referenceMap[i][j]] = rNF[referenceMap[i][j]]
		}

		for k := range reference {
			NF[reference[k]] = rNF[reference[k]]
		}

	}

	//fmt.Println("------------------------------------------")

	return NF

}

func Contains(a []string, x string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}
